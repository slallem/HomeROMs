/*

  HomeROMs

  Homemade (parallel) eprom programmer
  Dirty cheap and simple

FEATURES
- Relies on SERIAL COMMUNICATION (CLI / putty on com port !)
- 2x 595 shiftout for parallel address line 
- dataline as 8x arduino pins
- 8 bit DAC: 1x 595 shiftout + R2R DAC + LM358 opamp = tunable vpp power (0-25v)
- power circuit to provide 28v from 5v USB (using DC-DC converter module) 
- auto-test function (power system / vpp levels)
- first targets are 27C512 UV eprom and W27C512 eeprom
- modular design

 */

#include <Wire.h>
#include <avr/pgmspace.h> //for PROGMEM

//experimental
//#include <Adafruit_MCP4725.h>

// variable to receive data coming from the EERPROMs
byte data_received = 0; 

// eeprom address array, use I2C scanner sketch to find the addresses. 
// This array contains 2 chips with 4 device addresses (like virtual chips) each
//byte chip[] = {0x00,0x51,0x52,0x53,0x54,0x55,0x56,0x57}; 
//byte chip[] = {0x50, 0x51};  //24C04 = 2x 256BYTES

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

//DAC for VPP
int dacLatchPin = 10;//yellow, initial value 8// Pin connected to ST_CP of 74HC595
int dacClockPin = 12; //orange//Pin connected to SH_CP of 74HC595
int dacDataPin = 11; //white////Pin connected to DS of 74HC595
//--Adafruit_MCP4725 dac; // constructor

//serial 2x 595 for ADR
int adrLatchPin = A0;//yellow, initial value 8// Pin connected to ST_CP of 74HC595
int adrClockPin = dacClockPin; //orange//Pin connected to SH_CP of 74HC595
int adrDataPin = dacDataPin; //white////Pin connected to DS of 74HC595

//Digital pins 2 to 9 = data bus

int analogPin = A3;  //VPP reading

int activeVPPPin = 13; 

int chipEnablePin = A1; 

//EEProms erasure
int relayConnectA9ToVPP = A2; 

int isDebug = 0;

//INITIAL SETTINGS
int items_per_line = 16;
//W27C512
float SETTINGS_VPE = 14.0;
float SETTINGS_VPP = 12.0;

//fixed delays
#define DELAY_ENABLE_US        1000
#define DELAY_BEFORE_READ_US     10
#define DELAY_BEFORE_WRITE_US   100
#define DELAY_AFTER_WRITE_US    100
#define DELAY_ERASE_MS          500

#define MAXVAL   255
#define MAXVPP  25.0

#define ADIV_R1    47000
#define ADIV_R2    10000

#define AREF       5.0

void setup() {

  delay(200);

  analogReference(DEFAULT);
  //analogReference(EXTERNAL);
  //analogReference(INTERNAL);
  
  //Initialize serial
  //Serial.begin(9600);
  Serial.begin(115200);

  //experimental
  //dac.begin(0x62); // The I2C Address: Run the I2C Scanner if you're not sure
  
  //Reserve bytes for the inputString
  inputString.reserve(200);
  
  // waking up I2C
  Wire.begin();       
  delay(100);

  //vout dac pins
  pinMode(dacLatchPin, OUTPUT);
  pinMode(dacClockPin, OUTPUT);
  pinMode(dacDataPin, OUTPUT);

  // data pins 2..9
  setupDataPins(INPUT);

  //adress pins
  pinMode(adrLatchPin, OUTPUT);
  //--already set--pinMode(adrClockPin, OUTPUT);
  //--already set--pinMode(adrDataPin, OUTPUT);

  //!CE pin
  pinMode(chipEnablePin, OUTPUT);
  enableChip(false);

  //VPP switch
  pinMode(activeVPPPin, OUTPUT);
  digitalWrite(activeVPPPin, LOW);

  //"Erase" PIN
  //Connected to a relay to switch A9 directly to VPP (instead of address bus)
  pinMode(relayConnectA9ToVPP, OUTPUT);
  digitalWrite(relayConnectA9ToVPP, LOW);

  //Set VPP to 0v (dac)
  setVPPVoltage(0);

  //Welcome banner and initial prompt
  banner();
  help_reminder();
  prompt();
  
}

void setupDataPins(byte mode)
{
  // data pins
  pinMode(2, mode);
  pinMode(3, mode);
  pinMode(4, mode);
  pinMode(5, mode);
  pinMode(6, mode);
  pinMode(7, mode);
  pinMode(8, mode);
  pinMode(9, mode);

  if (mode == INPUT) {
    digitalWrite(2, 0);
    digitalWrite(3, 0);
    digitalWrite(4, 0);
    digitalWrite(5, 0);
    digitalWrite(6, 0);
    digitalWrite(7, 0);
    digitalWrite(8, 0);
    digitalWrite(9, 0);
  }
  delay(5);
}

#define MAX_STRING 60
char stringBuffer[MAX_STRING];
char* _getString(const char* str) {
  strcpy_P(stringBuffer, (char*)str);
  return stringBuffer;
}
#define _T(x) _getString(PSTR(x))

void banner() 
{
  Serial.println();
  Serial.println(_T("  _    _                      _____   ____  __  __     ")); 
  Serial.println(_T(" | |  | |                    |  __ \\ / __ \\|  \\/  |    "));
  Serial.println(_T(" | |__| | ___  _ __ ___   ___| |__) | |  | | \\  / |___ "));
  Serial.println(_T(" |  __  |/ _ \\| '_ ` _ \\ / _ \\  _  /| |  | | |\\/| / __|"));
  Serial.println(_T(" | |  | | (_) | | | | | |  __/ | \\ \\| |__| | |  | \\__ \\"));
  Serial.println(_T(" |_|  |_|\\___/|_| |_| |_|\\___|_|  \\_\\\\____/|_|  |_|___/"));
  Serial.println();
}

void prompt() {
  Serial.println();
  Serial.print("Ready> "); 
}

void help_reminder()
{
  Serial.println("Type 'help' for a list of available commands."); 
}

void help()
{
     if (isDebug)
       Serial.println("Debug mode is active");
     else
     Serial.println("Debug mode is not active");

  
      Serial.println(); 
      Serial.println(_T("This is the HELP page")); 
      Serial.println(_T("----------------------------------")); 

      
      Serial.println(_T("help         This page")); 
      Serial.println(_T("banner       N/D")); 
      
      Serial.println(_T("Low level commands")); 
      
      Serial.println(_T("vpp                   Read actual Vpp voltage")); 
      Serial.println(_T("vpp <value>           Set Vpp voltage (0.0 to 25.0)")); 
      
      Serial.println(_T("vpp on|off            Enable or disable Vpp on eprom socket"));
      Serial.println(_T("debug on|off          Verbose mode"));
      Serial.println(_T("adr <addr>            Move to address")); 
      
      Serial.println(_T("read                  Read byte at current address")); 
      Serial.println(_T("write <value>         Write byte at current address")); 

      Serial.println("ipl <value>           Read or set the number of items per line");
      
      Serial.println(_T("High level commands")); 
      
      Serial.println(_T("dump <adr>            Hex dump")); 
      Serial.println(_T("checksum <adr> <len>  Calculates 32 bit checksum")); 
      Serial.println(_T("burn a v v            Writes")); 
      
      Serial.println(_T("----------------------------------")) ;

     if (isDebug)
       Serial.println("Debug mode is active");
     else
     Serial.println("Debug mode is not active");

      
}

void cmdVPP(int argc, char **argv)
{
  if (argc == 1) // status = read real vpp voltage
  {
    readVPP();
  }
  else if (argc == 2) // set voltage
  {
    String arg = String(argv[1]);
    if (arg.equalsIgnoreCase("check")) {
      loop_vpp();
      return;
    }
    else if (arg.equalsIgnoreCase("on")) {
      digitalWrite(activeVPPPin, HIGH);
      return;
    }
    else if (arg.equalsIgnoreCase("off")) {
      digitalWrite(activeVPPPin, LOW);
      return;
    }
    
      float value = arg.toFloat();
      Serial.print("You want me to set VPP to ");
      Serial.print(value);
      Serial.println("v");

      setVPPVoltage(value);

      Serial.println("Done.");

      //delay_ms(50);
      readVPP();
      
  } else {
    Serial.println(_T("Bad command"));
  }
}

void cmdIPL(int argc, char **argv)
{
  if (argc == 1) 
  {
    Serial.print("items per line: ");
    Serial.print(items_per_line);
    Serial.println();
  }
  else if (argc == 2) // set pagesize
  {
    String arg = String(argv[1]);
    items_per_line = arg.toInt();
    Serial.print("items per line: ");
    Serial.print(items_per_line);
    Serial.println();
  } else {
    Serial.println(_T("Bad command"));
  }
}

byte getCharVal(char c)
{
   if(c >= '0' && c <= '9')
     return (byte)(c - '0');
   else
     return (byte)(c-'A'+10);
}

void cmdDecimal(int argc, char **argv)
{
  if (argc == 2) // hex reader
  {
    long value = parseHexa(argv[1]);
    
    Serial.print("result is: ");
    Serial.print(value);
    Serial.println();
  } else {
    Serial.println(_T("Bad command"));
  }
}

//utility function
long parseHexa(char *text)
{
    String str = String(text);
    str.toUpperCase();

    long value = 0;
    for(int i=0; i<str.length(); i++)
    {
      byte b = getCharVal(str.charAt(i));
      value = (value << 4) | b;
    }
    return value;
}

void cmdSetAddress(int argc, char **argv)
{
  if (argc == 1) 
  {
    Serial.println("Not yet implemented");
  }
  else if (argc == 2) // set voltage
  {
    //String arg = String(argv[1]);
    //int value = arg.toInt();
    int value = parseHexa(argv[1]);
    
    Serial.print("You want me to go to address ");
    Serial.println(value);

    //So, set it !
    goToADR(value);

    Serial.println("Done.");
  } else {
    Serial.println(_T("Bad command"));
  }
}

void cmdBurn(int argc, char **argv)
{
    if (argc > 2)
    {
        //--String arg = String(argv[1]);
        //--int addr_start = arg.toInt();
        int addr_start = parseHexa(argv[1]);

        enableChip(false);
        setupDataPins(OUTPUT);
        setVPPVoltage(SETTINGS_VPP);
        digitalWrite(activeVPPPin, HIGH); 
        delay(2);//waits for voltage?
        
        for (int i=0; i<argc-2; i++)  {
            //go to ...
            goToADR(addr_start + i);
            
            //--String arg = String(argv[2+i]);
            //--byte value = arg.toInt();
            byte value = parseHexa(argv[2+i]);
            
            writeByte(value);

            //enabling vpp = persistence of the value
            digitalWrite(chipEnablePin, LOW);//enabled
            delayMicroseconds(DELAY_BEFORE_WRITE_US);
            digitalWrite(chipEnablePin, HIGH);//disabled
            delayMicroseconds(DELAY_AFTER_WRITE_US);
        }

        setVPPVoltage(0);
        delay(5);

        //back to "normal" read mode
        setupDataPins(INPUT);

        enableChip(true);

        //check mode
        int bytesok = 0;
        int bytesko = 0;
        Serial.print("[checking]");
        for (int i=0; i<argc-2; i++)  {
            byte expected = parseHexa(argv[2+i]);
            //go to ...
            goToADR(addr_start + i);
            delayMicroseconds(DELAY_BEFORE_READ_US);
            byte b = readByte();
            Serial.print(" ");
            PrintHex8(&b,1);

            if (b == expected) {
              bytesok++;
            } else {
              bytesko++;
            }
        }

        enableChip(false);
        
        Serial.println();
        if (bytesko > 0) {
          Serial.print("** ERROR ** KO: ");
          Serial.print(bytesko);
          Serial.print(", OK: ");
          Serial.println(bytesok);
        }

        Serial.println("Done.");
    } else {
        Serial.println(_T("Bad command: Usage: burn <adress> <data> [<data> ... ]"));
    }
}

void cmdWrite(int argc, char **argv)
{
  if (argc == 2) // set value at current address
  {
    setVPPVoltage(SETTINGS_VPP);
    setupDataPins(OUTPUT);

    String arg = String(argv[1]);
    byte value = arg.toInt();
    writeByte(value);

    digitalWrite(activeVPPPin, HIGH); 
    delayMicroseconds(DELAY_BEFORE_WRITE_US);
    digitalWrite(activeVPPPin, LOW); 
    delayMicroseconds(DELAY_AFTER_WRITE_US);

    setupDataPins(INPUT);
    setVPPVoltage(0);

    Serial.println("Done.");
  } else {
    Serial.println(_T("Bad command"));
  }
}

void cmdErase(int argc, char **argv)
{
  Serial.println("*WARN* EEPROM erase mode");

  /*
    Winbond W27C512
    Erase Mode
    The erase operation is the only way to change data from "0" to "1." Unlike conventional UVEPROMs,
    which use ultraviolet light to erase the contents of the entire chip (a procedure that requires up to half
    an hour), the W27C512 uses electrical erasure. Generally, the chip can be erased within 100 mS by
    using an EPROM writer with a special erase algorithm.
    Erase mode is entered when OE/VPP is raised to VPE (14V), VCC = VCE (5V), A9 = VPE (14V), A0
    low, and all other address pins low and data input pins high. Pulsing CE low starts the erase
    operation.
   */

  enableChip(false);

  //VPP = VPE
  setVPPVoltage(SETTINGS_VPE);
  digitalWrite(activeVPPPin, HIGH); 

  //A9 = VPE
  digitalWrite(relayConnectA9ToVPP, HIGH);//set pin 9 to VPP (relay switched)

  //A0 low + all other address pins low
  goToADR(0);

  //Input pins high
  setupDataPins(OUTPUT);
  writeByte(0xFF);
  
  readVPP();

  // -- Pulsing CE low starts the erase operation

  enableChip(true);//!CE LOW
  delay(DELAY_ERASE_MS); //some wait
  enableChip(false); //!CE HIGH = end of erasure

  // -- finalization
  
  digitalWrite(activeVPPPin, LOW); 
  digitalWrite(relayConnectA9ToVPP, LOW);//set pin 9 back to address bus

  setVPPVoltage(0);
  digitalWrite(activeVPPPin, LOW); 

  readVPP();
  
  setupDataPins(INPUT);
  
  delay(DELAY_ERASE_MS);

  Serial.println("Erase procedure Done.");
}

void cmdRead(int argc, char **argv) {
  enableChip(true);
  byte val = readByte();
  enableChip(false);
  Serial.print("DATA READ=");
  Serial.println(val);
}

void cmdDump(int argc, char **argv)
{
  char mode = 'h';
  if (String(argv[0]).equalsIgnoreCase("asciidump")) {
    mode = 'a';
  }

  if (argc == 2 || argc == 3) 
  {
    int nbVal = (items_per_line > 0 ? items_per_line : 1 );
    //String arg = String(argv[1]);
    //int address = arg.toInt();
    long address = parseHexa(argv[1]);
    
    if (argc == 3) {
      nbVal = String(argv[2]).toInt();
    }

    doDump(address, nbVal, mode);
    
    Serial.println("Done.");
  } else {
    Serial.println(_T("Bad command: Usage: dump <adr> <len>"));
  }
}

void doDump(long address, int nbVal, char mode)
{
    setVPPVoltage(0);
    digitalWrite(activeVPPPin, LOW); 
    delay(2);//waits for no voltage?

    setupDataPins(INPUT);

    enableChip(true);

    PrintHex32(address);
    Serial.print(":");

    int nbw = 0;

    for (int i=0; i<nbVal; i++) {

      //page breaks
      nbw++;
      if ((items_per_line > 0) && (nbw > items_per_line)) {
        nbw = 1;
        Serial.println();
        PrintHex32(address + i);
        Serial.print(":");
      }
      
      goToADR(address + i);
      delayMicroseconds(DELAY_BEFORE_READ_US);
      byte b = readByte();
      Serial.print(" ");
      if (mode == 'd') {
        //decimal mode
        Serial.print(b);
      }
      else if (mode == 'a') {
        //text mode (ascii)
        if (b<32) {
          Serial.print(".");
        } else {
          Serial.print((char)b);
        }
      } else {
        //hex mode (default)
        PrintHex8(&b,1);
      } 
      
    }
    Serial.println();
    enableChip(false);
    
    Serial.println("Done.");
}

void cmdChecksum(int argc, char **argv)
{
  if (argc == 3) 
  {
    long address = parseHexa(argv[1]);
    int nbVal = String(argv[2]).toInt();
    doChecksum(address, nbVal);
  } else {
    Serial.println(_T("Bad command: Usage: checksum <adr> <len>"));
  }
}

void doChecksum(long address, int nbVal)
{
  setVPPVoltage(0);//reading only, no voltage needed for vpp
  enableChip(true);

  Serial.print("Checksum ");
  PrintHex32(address);
  Serial.print("-");
  PrintHex32(address + nbVal - 1);
  Serial.print(" = ");

  long chksum = 0;

  for (int i=0; i<nbVal; i++) {
    goToADR(address + i);
    delayMicroseconds(DELAY_BEFORE_READ_US);
    byte b = readByte();
    chksum += b;
  }

  PrintHex32(chksum);
  
  Serial.println();
  enableChip(false);
  
  Serial.println("Done.");
}

float readVPP() {
  int val = analogRead(analogPin);     // read the input pin

  if (isDebug) {
    Serial.print("ANALOGREAD=");
    Serial.println(val);
  }
  
  //voltage divider formula
  //vout = vin * ( R2 / (R1+R2));
  //i.e. vin = vout / (R2/(R1+R2));
  
  float voltage = float(val) / (float(ADIV_R2) / (float(ADIV_R1) + float(ADIV_R2))) * AREF / 1024.; 
  Serial.print("voltage=");
  Serial.print(voltage);
  Serial.println("v");

  return voltage;
}

byte readByte() {
  byte val = digitalRead (2);
  val |= digitalRead (3) << 1;
  val |= digitalRead (4) << 2;
  val |= digitalRead (5) << 3;
  val |= digitalRead (6) << 4;
  val |= digitalRead (7) << 5;
  val |= digitalRead (8) << 6;
  val |= digitalRead (9) << 7;
  return val;
}

void writeByte(byte val) {
  digitalWrite(2, val & 1);
  digitalWrite(3, (val >> 1) & 1);
  digitalWrite(4, (val >> 2) & 1);
  digitalWrite(5, (val >> 3) & 1);
  digitalWrite(6, (val >> 4) & 1);
  digitalWrite(7, (val >> 5) & 1);
  digitalWrite(8, (val >> 6) & 1);
  digitalWrite(9, (val >> 7) & 1);
}

void cmdDebug(int argc, char **argv)
{
  if (argc == 1) // status
  {
     if (isDebug)
       Serial.println("Debug mode is active");
     else
     Serial.println("Debug mode is not active");
  }
  else 
  {
    if (String(argv[1]).equalsIgnoreCase("on")) {
      isDebug = 1;
      Serial.println(_T("Debug mode activated"));
    } else if (String(argv[1]).equalsIgnoreCase("off")) {
      isDebug = 0;
      Serial.println(_T("Debug mode deactivated"));
    } else {
      Serial.println(_T("Unknown directive"));
    }
  }
}

void cmdScanI2C() {
  //see https://playground.arduino.cc/Main/I2cScanner
  Serial.println ();
  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;
  
  Wire.begin();
  for (byte i = 8; i < 120; i++)
  {
    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
      {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      count++;
      delay (1);  // maybe unneeded?
      } // end of good response
  } // end of for loop
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");
}

void cmdEcho(int arg_cnt, char **args)
{
  Serial.println(_T("echo works !"));
}

void loop() 
{
  // print the string when a newline arrives:
  if (stringComplete) 
  {
    String& cmd = inputString;
    cmd.trim();

    if (cmd == "") 
    {
      //nothing to do
      /*Serial.println("nothing to do");*/
    } else {
      parseCommandLine(cmd);
    }

    prompt(); 

    // clear the string:
    inputString = "";
    stringComplete = false;
  }
  
  //serialEvent();
}

void parseCommandLine(String cmd) {

//----------------------------------
//----------------------------------
//----------------------------------

    #define MAXARGS 32
    #define BUFSIZE 120
    uint8_t argc = 0;
    uint8_t i = 0;
    char *argv[MAXARGS];
    char buf[BUFSIZE];

    if (isDebug) {
      Serial.print("INPUT=");
      Serial.println(cmd.c_str());
    }

    // Peu intuitif ; à revoir 
    // parse the command line statement and break it up into space-delimited strings.
    // the array of strings will be saved in the argv array.
    argv[i] = strtok(cmd.c_str(), " ");
    do
    {
        argv[++i] = strtok(NULL, " ");
    } while ((i < MAXARGS) && (argv[i] != NULL));

    //nombre d'arguments (non nulls)
    argc = i;

    if (isDebug) {
      Serial.print("ARGC=");
      Serial.println(argc);
      for (i = 0; i<argc; i++) {
        Serial.print("ARG[");
        Serial.print(i);
        Serial.print("]=");
        Serial.print(argv[i]);
        Serial.println();
      }
    }

//----------------------------------
//----------------------------------
//----------------------------------
      
      if (cmd == "vpp")  
      {
        cmdVPP(argc, argv);
      }
      else if (cmd == "acheck")  
      {
        loop_adr();
      }
      else if (cmd == "i2c") {
        cmdScanI2C();
      }
      else if (cmd == "lightshow")  
      {
        lightshow();
      }
      else if (cmd == "ipl")  
      {
        cmdIPL(argc, argv);
      }
      else if (cmd == "read")  
      {
        cmdRead(argc, argv);
      }
      else if (cmd == "dec")  
      {
        cmdDecimal(argc, argv);
      }
      else if (cmd == "write")  
      {
        cmdWrite(argc, argv);
      }
      else if (cmd == "burn")  
      {
        cmdBurn(argc, argv);
      }
      else if (cmd == "adr")  
      {
        cmdSetAddress(argc, argv);
      }
      else if (cmd == "debug")  
      {
        cmdDebug(argc, argv);
      }
      else if (cmd == "banner") 
      {
        banner();
      }
      /*
      else if (cmd == "light") 
      {
         if (ledstate == LOW)
           Serial.println("Light is off");
         else
         Serial.println("Light is on");
      }
      else if (cmd == "light off") 
      {
        ledstate = LOW;
         digitalWrite(ledPin, ledstate); 
      }
      else if (cmd == "light on") 
      {
        ledstate = HIGH;
         digitalWrite(ledPin, ledstate); 
      }*/
      
      else if (cmd == "test") 
      {
        //loop_vpp();
        int v1 = 0xffffffff;
        Serial.println("TEST1=");
        PrintHex32(v1);
        Serial.println();
        Serial.println("TEST2=");
        PrintHex32(0xffffffff);
        Serial.println();
        Serial.println("TEST3=");
        PrintHex32(0xffff);
        Serial.println();
      }
      
      else if (cmd == "erase") {
        cmdErase(argc, argv);
      }
      else if (cmd == "d") {
        doDump(0, 16, "h");
      }
      else if (cmd == "dump" || cmd == "asciidump") {
        cmdDump(argc, argv);
      }
      else if (cmd == "checksum") {
        cmdChecksum(argc, argv);
      }
      /*
      else if (cmd == "sdump") 
      {
        eprom_dump();
      }
      else if (cmd == "shexdump") 
      {
        eprom_hexdump();
      }
      else if (cmd == "scx4200reset") 
      {
        eprom_write_SCX4200RESET();
      }
      */
      else if (cmd == "help") 
      {
        help();
      }
      else if (cmd[0] == '+') 
      {
        //echo
        Serial.println(cmd); 
      }
      else if (cmd[0] == '*') 
      {
        //echo
        for (int i = 0; i < cmd.length() - 1; i++){
          Serial.print(i, DEC);
          Serial.print(" = ");
          Serial.print(String((byte)cmd[i]));
          Serial.println();
        }
      }
      else
      {
        Serial.println("Unknown command. Type 'help' for a list of available commands."); 
      }

}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available() > 0) {

    // get the new byte:
    char inChar = (char)Serial.read(); 

    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n' || inChar == '\r') {
        Serial.write("\r\n"); 
        stringComplete = true;
    } 
    else if (inChar==3) {
        //^C
        Serial.println("^C"); 
        //do nothing
        inputString = "";
        stringComplete = true;
    }
    else if (inChar < 32 || inChar > 127) {
       //String stmp = "[" + String((byte)inChar) + "]";
       //inputString += stmp;
       //KO
    }
    else if (inChar == 127) {
      if (inputString.length()>0)
      {
        inputString = inputString.substring(0, inputString.length() - 1);
        Serial.write(inChar); //echo printable
      }
    }
    else if (inChar >= 32 && inChar <127)
    {
      //any other printable char
      // add it to the inputString:
      inputString += inChar;
      Serial.write(inChar); //echo printable
    }
  }
}

void writeData(int device_address, unsigned int memory_address, byte data) 
// write one byte of data 'data' to eeprom memory address 'memory_address' to  with I2C address 'device_address'
{
  Wire.beginTransmission(device_address);  // device address
  Wire.write(memory_address );             // memory address
  Wire.write(data);                        // data to send
  Wire.endTransmission();                  // end
  delay(10);
}

byte readData(int device_address, unsigned int memory_address) 
// reads one byte of data from memory location 'memory_address' in chip at I2C address 'device_address' 
{
  byte result;  // return value

  Wire.beginTransmission(device_address); // device address
  Wire.write(memory_address);             // memory address
  Wire.endTransmission();                 // end
  Wire.requestFrom(device_address,1);     // get one byte of data from device
  if (Wire.available()) 
    result = Wire.read();
  return result;                          // return the read data byte
  delay(10);
}

void PrintHex8(uint8_t *data, uint8_t length) // prints 8-bit data in hex with leading zeroes
{
  for (int i=0; i<length; i++) { 
    if (data[i]<0x10) {
      Serial.print("0");
    } 
    Serial.print(data[i],HEX); 
  }
}

void PrintHex8(byte data) // prints 8-bit data in hex with leading zeroes
{
    if (data<0x10) {
      Serial.print("0");
    } 
    Serial.print(data,HEX); 
}

void PrintHex32(unsigned long data) // prints 32-bit data in hex with leading zeroes
{
  PrintHex8(data >> 24);
  PrintHex8(data >> 16);
  PrintHex8(data >> 8);
  PrintHex8(data);
}

void PrintBits(byte data) // prints 32-bit data in hex with leading zeroes
{
  print_binary(data, 8);
}

void print_binary(int v, int num_places)
{
    int mask=0, n;

    Serial.print("0b");
    for (n=1; n<=num_places; n++)
    {
        mask = (mask << 1) | 0x0001;
    }
    v = v & mask;  // truncate v to specified number of places

    while(num_places)
    {

        if (v & (0x0001 << num_places-1))
        {
             Serial.print("1");
        }
        else
        {
             Serial.print("0");
        }

        --num_places;
        /*
        if(((num_places%4) == 0) && (num_places != 0))
        {
            Serial.print("_");
        }*/
    }
}

/*
byte razrom1[] PROGMEM = {
0x43, 0x34, 0x32, 0x30, 0x30, 0x45, 0x58, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
};
*/

void loop_vpp() {
  Serial.println("LOOPING 0 to 255");   
  for (int j = 0; j <= 255; j++) {
    writeToDAC(j);
    Serial.println(j,HEX);   
    delay(100);
  }
  Serial.println("DONE.");   
}

void loop_adr() {
  Serial.println("LOOPING 0 to 8192");   
  for (long j = 0; j <= 8192; j++) {
    goToADR(j);
    //Serial.println(j,HEX);   
    PrintHex32(j);
  }
  Serial.println("DONE.");   
}

void lightshow() {
  for (int i=0; i<5; i++) {
    lightblink();
  }
  for (int i=0; i<4; i++) {
    lightbits();
  }
  for (int i=0; i<5; i++) {
    lightblink();
  }
  Serial.println("DONE.");
}

void lightbits() {
  //Moves one bit side from side 
  byte att = 100;
  long adr = 1;
  for (long j=0; j<15; j++) {
    goToADR(adr);
    delay(att);
    adr = adr << 1;
  }
  goToADR(adr);
  delay(att);
  for (long j=0; j<15; j++) {
    adr = adr >> 1;
    goToADR(adr);
    delay(att);
  }
}

void lightblink() {
  //0 then -1
  goToADR(0);
  delay(300);
  goToADR(-1);
  delay(300);
}


void setVPPVoltage(float value) {
      if (value > MAXVPP) {
        Serial.println("*ERROR* value exceeds maximum voltage");
        return;
      }

      //calculate byte value
      int valueToSet = (value * MAXVAL) / MAXVPP;
      if (valueToSet > 255) {
        valueToSet = 255;
      }

      if (isDebug) {
          Serial.print("That gives int8 value = ");
          Serial.print(valueToSet);
          Serial.print(" (");
          PrintBits(valueToSet);
          Serial.println(")");
          float vdac = (5.0 * valueToSet) / 255.0;
          Serial.print("(should give ");
          Serial.print(vdac);
          Serial.println("v on DAC output)");
      }

      //Set it !
      writeToDAC(valueToSet);

      /*
      // --- MCP4725 DAC version
      int dac_value = (value * 4095) / MAXVPP;
      if (dac_value > 4095) {
        dac_value = 4095;
      }
      dac.setVoltage(dac_value, false);
      */

      delay(10);
}


void writeToDAC(byte value) {
    // --- Homemade 595+R2R DAC
    //ground latchPin and hold low for as long as you are transmitting
    digitalWrite(dacLatchPin, LOW);
    //shiftOut(dacDataPin, dacClockPin, LSBFIRST, value);
    shiftOut(dacDataPin, dacClockPin, MSBFIRST, value);
    //return the latch pin high to signal chip that it
    //no longer needs to listen for information
    digitalWrite(dacLatchPin, HIGH);
}

void goToADR(long value) {
    //ground latchPin and hold low for as long as you are transmitting
    digitalWrite(adrLatchPin, LOW);
    
    //8 bit address (256B)
    digitalWrite(adrClockPin, LOW);
    shiftOut(adrDataPin, adrClockPin, LSBFIRST, value);
    //16 bit address (64KB)
    shiftOut(adrDataPin, adrClockPin, LSBFIRST, (value >> 8));
    //24 bit address (16MB)
    //3x 595 : shiftOut(adrDataPin, adrClockPin, LSBFIRST, (value >> 16) & 0xFF);
    //32 bit address (4GB)
    //4x 595 : shiftOut(adrDataPin, adrClockPin, LSBFIRST, (value >> 24) & 0xFF);
    digitalWrite(adrDataPin, LOW);

    //return the latch pin high to signal chip that it
    //no longer needs to listen for information
    digitalWrite(adrLatchPin, HIGH);
}


void enableChip(boolean bEnable) {
    digitalWrite(chipEnablePin, (bEnable) ? LOW : HIGH);
    delayMicroseconds(DELAY_ENABLE_US);
}

