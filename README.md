HomeROMs

An experimental homemade parallel eprom programmer
Cheap and simple
Based on arduino uno

FEATURES
- Relies on SERIAL COMMUNICATION (CLI / putty on com port !)
- 2x 595 shiftout for parallel address line 
- Data Line as 8x arduino pins
- 8 bit DAC: 1x 595 shiftout + R2R DAC + LM358 opamp = tunable vpp power (0-25v)
- power circuit to provide 28v from 5v USB (using DC-DC converter module) 
- auto-test function (power system / vpp levels)
- first targets are 27C512 UV eprom and W27C512 eeprom
- modular design
